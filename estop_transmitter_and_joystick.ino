#define BROADCAST_INTERVAL_MILLISECONDS 50
#define BUTTON_PRESS_DEBOUNCE_TIMER     100
#define BUTTON_RELEASE_DEBOUNCE_TIMER   250

#define JOYSTICK_X_AXIS_ANALOG_PIN    A0
#define JOYSTICK_Y_AXIS_ANALOG_PIN    A1
#define JOYSTICK_Z_AXIS_ANALOG_PIN    A2
#define JOYSTICK_BUTTON_1_DIO         A4
#define JOYSTICK_BUTTON_2_DIO         A3
#define ENABLE_SWITCH_DIO             12
#define STATUS_LED                    13

#define JOYSTICK_X_AXIS_MIN           5
#define JOYSTICK_X_AXIS_MID           512
#define JOYSTICK_X_AXIS_MAX           1015

#define JOYSTICK_Y_AXIS_MIN           28
#define JOYSTICK_Y_AXIS_MID           522
#define JOYSTICK_Y_AXIS_MAX           1015

#define JOYSTICK_Z_AXIS_MIN           3
#define JOYSTICK_Z_AXIS_MID           510
#define JOYSTICK_Z_AXIS_MAX           1020

#define JOYSTICK_DEADBAND             20.0f
#define MULE_VELOCITY_LIMIT           45.0f //actual mule max speed in Rad/s...max of 1000RPM allowed at motor with gear ratio of 300/14

unsigned long lastBroadcast = 0;
unsigned long lastButtonPressTime = 0;
unsigned long lastButtonReleaseTime = 0;
float sinWave = 0.0f;
int joystick_X_Axis_Mapped, joystick_Y_Axis_Mapped, joystick_Z_Axis_Mapped;
int joystick_button_1, joystick_button_2, enable_switch;
int leftWheelVelocityRequest, rightWheelVelocityRequest;
int powerLEDToggle = 0;

void setup() 
{
  Serial.begin(9600);
  Serial1.begin(9600);

  pinMode(ENABLE_SWITCH_DIO, INPUT_PULLUP);
  pinMode(JOYSTICK_BUTTON_1_DIO, INPUT_PULLUP);
  pinMode(JOYSTICK_BUTTON_2_DIO, INPUT_PULLUP);
  pinMode(STATUS_LED, OUTPUT);
}

void loop() 
{ 
  joystick_Y_Axis_Mapped = joyMapWithDead(analogRead(JOYSTICK_Y_AXIS_ANALOG_PIN), JOYSTICK_Y_AXIS_MIN, JOYSTICK_Y_AXIS_MID, JOYSTICK_Y_AXIS_MAX, -MULE_VELOCITY_LIMIT, MULE_VELOCITY_LIMIT, JOYSTICK_DEADBAND);
  joystick_Y_Axis_Mapped = constrain(joystick_Y_Axis_Mapped, -MULE_VELOCITY_LIMIT, MULE_VELOCITY_LIMIT);

  joystick_X_Axis_Mapped = joyMapWithDead(analogRead(JOYSTICK_X_AXIS_ANALOG_PIN), JOYSTICK_X_AXIS_MIN, JOYSTICK_X_AXIS_MID, JOYSTICK_X_AXIS_MAX, -MULE_VELOCITY_LIMIT, MULE_VELOCITY_LIMIT, JOYSTICK_DEADBAND); 
  joystick_X_Axis_Mapped = constrain(joystick_X_Axis_Mapped, -MULE_VELOCITY_LIMIT, MULE_VELOCITY_LIMIT);

  joystick_Z_Axis_Mapped = joyMapWithDead(analogRead(JOYSTICK_Z_AXIS_ANALOG_PIN), JOYSTICK_Z_AXIS_MIN, JOYSTICK_Z_AXIS_MID, JOYSTICK_Z_AXIS_MAX, -MULE_VELOCITY_LIMIT, MULE_VELOCITY_LIMIT, JOYSTICK_DEADBAND);
  joystick_Z_Axis_Mapped = constrain(joystick_Z_Axis_Mapped, -MULE_VELOCITY_LIMIT, MULE_VELOCITY_LIMIT);

  leftWheelVelocityRequest = joystick_Y_Axis_Mapped + joystick_Z_Axis_Mapped;
  rightWheelVelocityRequest = joystick_Y_Axis_Mapped - joystick_Z_Axis_Mapped;

  joystick_button_1 = !digitalReadFast(JOYSTICK_BUTTON_1_DIO);
  joystick_button_2 = !digitalReadFast(JOYSTICK_BUTTON_2_DIO);

  if (!digitalReadFast(ENABLE_SWITCH_DIO))
  {
    if(lastButtonPressTime > BUTTON_PRESS_DEBOUNCE_TIMER)
    {
      enable_switch = 1;
      lastButtonPressTime = 0;
    }
    else
    {
      lastButtonPressTime ++;
    }
  }  

  if (digitalReadFast(ENABLE_SWITCH_DIO))
  {
    if(lastButtonReleaseTime > BUTTON_RELEASE_DEBOUNCE_TIMER)
    {
      enable_switch = 0;
      lastButtonReleaseTime = 0;
    }
    else
    {
      lastButtonReleaseTime ++;
    }
  }
  
  if ((millis() - lastBroadcast) > BROADCAST_INTERVAL_MILLISECONDS) 
  {   
    //blink main power LED at 50ms
    powerLEDToggle = !powerLEDToggle;
    digitalWrite(STATUS_LED, powerLEDToggle);
    
    sendState();
  } 
}

void sendState() 
{
  //Send data out of the USB port for debug
  Serial.print('H');
  Serial.print(',');
  Serial.print(enable_switch);//digitalRead(ENABLE_SWITCH_DIO) ? "0" : "1");
  Serial.print(',');
  Serial.print(joystick_button_1);//digitalRead(JOYSTICK_BUTTON_1_DIO) ? "0" : "1");
  Serial.print(',');
  Serial.print(joystick_button_2);//digitalRead(JOYSTICK_BUTTON_2_DIO) ? "0" : "1");
  Serial.print(',');  
  Serial.print(leftWheelVelocityRequest);
  Serial.print(',');
  Serial.println(rightWheelVelocityRequest);
  //Serial.print(',');
  //Serial.print(analogRead(JOYSTICK_Y_AXIS_ANALOG_PIN));
  //Serial.print(',');
  //Serial.println(analogRead(JOYSTICK_Z_AXIS_ANALOG_PIN));
  
  //Send data out of the radio to mule
  Serial1.print('H');
  Serial1.print(',');
  Serial1.print(enable_switch);//digitalRead(ENABLE_SWITCH_DIO) ? "0" : "1");
  Serial1.print(',');
  Serial1.print(joystick_button_1);//digitalRead(JOYSTICK_BUTTON_1_DIO) ? "0" : "1");
  Serial1.print(',');
  Serial1.print(joystick_button_2);//digitalRead(JOYSTICK_BUTTON_2_DIO) ? "0" : "1");
  Serial1.print(',');  
  Serial1.print(leftWheelVelocityRequest);
  Serial1.print(',');
  Serial1.println(rightWheelVelocityRequest);
  
  lastBroadcast = millis();
}

void buttonChange() 
{
  sendState();
}

float joyMapWithDead(float joystickData, float joystickLowPos, float joystickCenterPos, float joystickHighPos, float MinOut, float MaxOut, float deadBand)
{
  if(joystickData > joystickCenterPos + deadBand)
  {
    joystickData = mapFloat(joystickData, joystickCenterPos+deadBand, joystickHighPos, 0.0f, MaxOut);
    joystickData = constrain(joystickData, 0.0f, MaxOut);
    return joystickData;
  }
  else if(joystickData < joystickCenterPos-deadBand)
  {
    joystickData = mapFloat(joystickData, joystickLowPos, joystickCenterPos-deadBand, MinOut, 0.0f);
    joystickData = constrain(joystickData, MinOut, 0.0f);
    return joystickData;
  }
  else
  {
     joystickData = 0.0f;
     return joystickData;
  }
}

float mapFloat(float x, float in_min, float in_max, float out_min, float out_max)
{
 return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}



